
var position1;
var position2;
var position3;
var waypoints = [];
var i = 0;

function initMap() {
    var from = {lat: 52, lng: 21};
    var directionsDisplay = new google.maps.DirectionsRenderer;
    var directionsService = new google.maps.DirectionsService;
    var map = new google.maps.Map(document.getElementById('map'), {
        zoom: 8,
        center: from
    });
    directionsDisplay.setMap(map);
    var geocoder = new google.maps.Geocoder();

    document.getElementById('submit').addEventListener('click', function() {
        click();
    });

    calculateAndDisplayRoute(directionsService, directionsDisplay);
    document.getElementById('mode').addEventListener('change', function() {
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    });


    function calculateAndDisplayRoute(directionsService, directionsDisplay) {
        var selectedMode = document.getElementById('mode').value;
        directionsService.route({
            origin: position1,  // Haight.
            destination: position2,  // Ocean Beach.
            //waypoints: position3,
            //optimalizeWayPoints: true,
            // Note that Javascript allows us to access the constant
            // using square brackets and a string value as its
            // "property."
            travelMode: google.maps.TravelMode[selectedMode]
        }, function(response, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                directionsDisplay.setDirections(response);
            } else {
                window.alert('Directions request failed due to ' + status);
            }
        });
    }

    function click (){
        geocodeAddress(geocoder, map, 'from');
        geocodeAddress(geocoder, map, 'to');
        geocodeAddress(geocoder, map, 'by');
        calculateAndDisplayRoute(directionsService, directionsDisplay);
    }
}

function geocodeAddress(geocoder, resultsMap, div) {
    var address = document.getElementById(div).value;
    geocoder.geocode({'address': address}, function(results, status) {
        if (status === google.maps.GeocoderStatus.OK) {
            resultsMap.setCenter(results[0].geometry.location);
            var marker = new google.maps.Marker({
                map: resultsMap,
                position: results[0].geometry.location
            });
            if(div == 'from')
                position1 = results[0].geometry.location;
            else if(div == 'to')
                position2 = results[0].geometry.location;
            else if(div == 'by') {
                //if(position3[i] != null)
                //    i++;
                position3 = results[0].geometry.location;
            }
        } else {
            alert('Geocode was not successful for the following reason: ' + status);
        }
    });

}

function getTrace(){
    var service = new google.maps.DistanceMatrixService();
    var selectedMode = document.getElementById('mode').value;

    service.getDistanceMatrix(
        {
            origins: [position1],
            destinations: [position2],
            travelMode: google.maps.TravelMode[selectedMode],
            avoidHighways: false,
            avoidTolls: false
        },
        callback
    );
    function callback(response, status) {
        var orig = document.getElementById("orig"),
            dest = document.getElementById("dest"),
            dist = document.getElementById("dist"),
            dura = document.getElementById("dura");

        if (status == "OK") {
            orig.value = response.destinationAddresses[0];
            dest.value = response.originAddresses[0];
            dist.value = response.rows[0].elements[0].distance.text;
            dura.value = response.rows[0].elements[0].duration.text;
        } else {
            alert("Error: " + status);
        }


    }
}
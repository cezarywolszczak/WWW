<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Mapa Google</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="googleMapsInit.js" type="text/javascript"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>

</head>
<body onload="initMap()">
<div class="header" style=" width: 400px">
    <a href="../index.html">
        <img src="../logo.png" alt="">
    </a>
    Mapa Google
</div>

<div id="container">

    <div id="inputsDiv">
        <table id="input">
            <tr>
                <td>Z:</td>
                <td>Do:</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <input id="from" name="from" type="text" value="Łódź" title="Z">
                </td>
                <td>
                    <input id="to" name="to" type="text" value="Warszawa" title="Do" >
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td><td></td><td>&nbsp;</td>
            </tr>
            <tr>
                <td>Punkt przelotowy:</td>
                <td>Rodzaj podróży:</td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <input id="by" name="by" type="text" title="Przez" value="Radom">
                </td>
                <td>
                    <select id="mode" onchange="getTrace()">
                        <option value="DRIVING">Jazda</option>
                        <option value="BICYCLING">Jazda na roweże</option>
                        <option value="WALKING">Wędrówka piesza</option>
                        <option value="TRANSIT">Trasa tranzytowa</option>
                    </select>
                </td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td></td>
                <td>
                    <input type="submit" value="Wyszukaj połączenia" id="submit" onclick="getTrace()">
                </td>

            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>Miejsce startu:</td>
                <td colspan="2">
                    <input id="dest" type="text" style="width:300px; background-color: #303030; border: none; color: white" title="">
                </td>
            </tr>
            <tr>
                <td>Miejsce docelowe:</td>
                <td colspan="2" >
                    <input id="orig" type="text" style="width:300px; background-color: #303030; border: none; color: white" title="">
                </td>
            </tr>
            <tr>
                <td>Dystans:</td>
                <td colspan="2">
                    <input id="dist" type="text" style="width:300px; background-color: #303030; border: none; color: white" title="">
                </td>
            </tr>
            <tr>
                <td>Czas:</td>
                <td colspan="2">
                    <input id="dura" type="text" style="width:300px; background-color: #303030; border: none; color: white" title="">
                </td>
            </tr>
        </table>
    </div>

    <div id="map">

    </div>
    <div class="footer" style="margin-top: 30px">
        <br/><br/>
        <a href="https://validator.w3.org/nu/?useragent=Validator.nu%2FLV+http%3A%2F%2Fvalidator.w3.org%2Fservices&doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F%7Ewolszczc%2FGoogleMap%2FgoogleMapsPage.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="https://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F%7Ewolszczc%2FGoogleMap%2FgoogleMapsPage.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/GoogleMap/googleMapsPage.php" target="_blank">googleMapsPage.php</a>&nbsp;
        <a class="hreff" href="http://volt.iem.pw.edu.pl/~wolszczc/GoogleMap/googleMapsInit.js" target="_blank">googleMapsInit.js</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/GoogleMap/style.css" target="_blank">style.css</a>
    </div>
</div>

</body>

</html>




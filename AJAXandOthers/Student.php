<?php

/**
 * Created by PhpStorm.
 * User: HP
 * Date: 29.12.2015
 * Time: 13:41
 */
class Student extends Osoba
{
    private $ocena;

    public function getOcena()
    {
        return $this->ocena;
    }

    public function setOcena($ocena)
    {
        $this->ocena = $ocena;
    }

    public function __construct($imie, $nazwisko, $pesel, $ocena)
    {
        parent::__construct($imie, $nazwisko, $pesel);
        $this->ocena = $ocena;
    }

    public function __destruct()
    {
        parent::__destruct();
        unset($this);
    }

    public function wypisz()
    {
        parent::wypisz();
        echo "Ocena: " . $this->getOcena() . "<br>";
    }

}


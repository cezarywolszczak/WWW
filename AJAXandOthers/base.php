<?php
include 'DatabaseLogin.php';

$login = array();
$login [] = new DatabaseLogin("Adam", "Adamiak","adek");
$login [] = new DatabaseLogin("Bartek", "Brzoza","baro");
$login [] = new DatabaseLogin("Cyprian", "Cieślak","123");
$login [] = new DatabaseLogin("Zbyszek", "Zaręba","zbychu");
$login [] = new DatabaseLogin("Zbyszek", "Wrona","zbychu2");
$login [] = new DatabaseLogin("Bartek", "Kowalski","bartek");

$matches = array();

if(isset($_GET['q']) ? $_GET['q'] : '') {
    $q = $_GET['q'];

    foreach ($login as $user) {
        if (strpos($user->login, $q) === 0)
            $matches [] = $user;
    }
}

echo json_encode($matches);
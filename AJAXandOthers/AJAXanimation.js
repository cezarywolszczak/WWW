/**
 * Created by HP on 31.12.2015.
 */

function animation() {
    var canvas = document.getElementById("animation");
    var c = canvas.getContext("2d");
    var posX = 0;
    var posY = 0;
    var posXFlag = true;
    var posYFlag = true;

    setInterval(function () {

        if (posXFlag == true) {
            posX += 3;
            if (posX > canvas.width - 60)
                posXFlag = false;
        } else {
            posX -= 3;
            if (posX < 10)
                posXFlag = true;
        }

        if (posYFlag == true) {
            posY += 3;
            if (posY > canvas.height - 60)
                posYFlag = false;
        } else {
            posY -= 3;
            if (posY < 10)
                posYFlag = true;
        }

        c.fillStyle = "#303030";
        c.fillRect(0, 0, canvas.width, canvas.height);

        c.fillStyle = "#007F7F";
        c.fillRect(posX, posY, 50, 50);

        c.strokeStyle = "black";
        c.lineWidth = 3;
        c.strokeRect(posX, posY, 50, 50);

    }, 30);

}

function synchRequest(isSync, method) {
    if (typeof XMLHttpRequest == "undefined") {
        XMLHttpRequest = function () {
            return new ActiveXObject(
                navigator.userAgent.indexOf("MSIE 5") >= 0 ? "Microsoft.XMLHTTP" : "Msxml2.XMLHTTP"
            );
        }
    }
    var xml = new XMLHttpRequest();
    xml.onreadystatechange = function () {
        if (xml.readyState == 4 && xml.status == 200) {
        }
    };
    xml.open(method, "source.php", isSync);
    xml.send();
}

function onload() {
    animation();
}


function showHint(str) {
    var hints = document.getElementById('complement');
    hints.innerHTML = '';


    if (str.length != 0) {
        var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function () {

            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {

                var matches = JSON.parse(xmlhttp.responseText);
                for (var i = 0; i < matches.length; i++) {
                    var li = document.createElement('li');
                    var t = document.createTextNode(matches[i].login + '(' + matches[i].name + ' ' + matches[i].surname + ')');
                    li.appendChild(t);
                    li.addEventListener('click', function () {
                        fillForm(this.innerHTML);
                    });
                    hints.appendChild(li);
                }
            }
        };
        xmlhttp.open("GET", "base.php?q=" + str, true);
        xmlhttp.send();
    }
}

function fillForm(str) {

    var loginField = document.getElementById('login');
    var hiddenName = document.getElementById('name');
    var hiddenSurname = document.getElementById('surname');
    loginField.value = str.substring(0, str.indexOf('('));
    hiddenName.value = str.substring(str.indexOf('(') + 1, str.indexOf(' '));
    hiddenSurname.value = str.substring(str.indexOf(' ') + 1, str.indexOf(')'));

    document.getElementById('complement').innerHTML = '';

}

document.getElementById('login').addEventListener('keyup', function () {
    showHint(this.value);
});


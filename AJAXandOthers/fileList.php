<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Lista plików</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>


</head>
<body>

<div class="header">
    <a href="../index.html">
        <img src="../logo.png"  alt="">
    </a>
    Lista plików
</div>

<div id="container">

<?php
$log_directory = 'upload';

$results_array = array();

if (is_dir($log_directory)) {
    if ($handle = opendir($log_directory)) {
        while(($file = readdir($handle)) !== FALSE) {
            $results_array[] = $file;
        }
        closedir($handle);
    }
}
?>

<table id="fileList">
    <tr>
        <td>Lp.</td>
        <td>Nazwa pliku</td>
        <td>Rozmiar pliku(bajtów)</td>
        <td>Typ pliku</td>
    </tr>
<?php
$i = 0;
foreach($results_array as $value)
{
    $i++;
    echo "<tr>".
            "<td>".
                "$i".
            "</td>".
            "<td class='fileName'>".
                "<a href='download.php?filename=upload/$value' class='fileHref'>".$value."</a>".
            "</td>".
            "<td>".
                filesize('upload/'.$value).
            "</td>".
            "<td>".
                mime_content_type('upload/'.$value).
            "</td>".
         "</tr>";
}
?>
</table>
</div>

<div class="footer">
    <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2FfileList.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
    <a href="https://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2FfileList.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
    <br/>Źródła strony:<br/>
    <a class="hreff" href="view.php?file=fileList.php" target="_blank">fileList.php</a>&nbsp;
    <a class="hreff" href="view.php?file=download.php" target="_blank">download.php</a>&nbsp;
    <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/style.css" target="_blank">style.css</a>
</div>

</body>

</html>


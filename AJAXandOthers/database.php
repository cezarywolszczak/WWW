<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Baza danych</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>

</head>
<body onload="onload()">
<div class="header" style=" width: 350px">
    <a href="../index.html">
        <img src="../logo.png" alt="">
    </a>
    Baza danych
</div>

<div id="container">
    <div class="objectResult">
        <div id="inputField">
            <form action="" method="post">
                <label for="login">Wpisz login: </label><br>
                <input type="text" name="login" id="login" title="Wyszukaj">
                <input type="submit" value="Prześlij">
                <div>
                    <ul id="complement">
                    </ul>
                </div>
                <input type="hidden" id="name" name="name">
                <input type="hidden" id="surname" name="surname">
            </form>
            <script src="databaseAJAX.js" type="text/javascript"></script>

        </div>
    </div>
    <div class="footer" style="margin-top: 30px">
        <br/><br/>
        <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2Fdatabase.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="https://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2Fdatabase.phpp"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/database.php" target="_blank">database.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/databaseAJAX.js" target="_blank">databaseAJAX.js</a>&nbsp;
        <a class="hreff" href="view.php?file=DatabaseLogin.php" target="_blank">DatabaseLogin.php</a>&nbsp;
        <a class="hreff" href="view.php?file=base.php" target="_blank">base.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/style.css" target="_blank">style.css</a>
    </div>
</div>

</body>

</html>




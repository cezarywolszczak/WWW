<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>AJAX synchroniczny i asynchroniczny</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <script src="AJAXAnimation.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>

</head>
<body onload="onload()">
<div class="header" style=" width: 500px">
    <a href="../index.html">
        <img src="../logo.png" alt="">
    </a>
    AJAX synchroniczny i asynchroniczny
</div>

<div id="container">
    <div class="objectResult">
        <canvas width="700" height="350" id="animation"></canvas>
        <input type="submit" name="sync" value="Aynchronicznie GET &nbsp;" onclick="synchRequest(true,'GET')">
        <input type="submit" name="async" value="Synchronicznie GET &nbsp;&nbsp;" onclick="synchRequest(false,'GET')">
        <br><br>
        <input type="submit" name="sync" value="Aynchronicznie POST" onclick="synchRequest(true,'POST')">
        <input type="submit" name="async" value="Synchronicznie POST" onclick="synchRequest(false,'POST')">

    </div>
    <div class="footer" style="margin-top: 30px">
        <br/><br/>
        <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2FAJAXSyncAsync.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="https://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2FAJAXSyncAsync.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/AJAXSyncAsync.php" target="_blank">AJAXSyncAsync.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/AJAXAnimation.js" target="_blank">AJAXSyncAsync.php</a>&nbsp;
        <a class="hreff" href="view.php?file=source.php" target="_blank">source.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/style.css" target="_blank">style.css</a>
    </div>
</div>

</body>

</html>




<?php

/**
 * Created by PhpStorm.
 * User: HP
 * Date: 29.12.2015
 * Time: 13:41
 */
class Osoba
{
    private $imie;
    private $nazwisko;
    private $pesel;

    public function getImie()
    {
        return $this->imie;
    }

    public function getNazwisko()
    {
        return $this->nazwisko;
    }

    public function getPesel()
    {
        return $this->pesel;
    }

    public function setImie($imie)
    {
        $this->imie = $imie;
    }

    public function setNazwisko($nazwisko)
    {
        $this->nazwisko = $nazwisko;
    }

    public function setPesel($pesel)
    {
        $this->pesel = $pesel;
    }


    public function __construct($imie, $nazwisko, $pesel)
    {
        $this->imie = $imie;
        $this->nazwisko = $nazwisko;
        $this->pesel = $pesel;
    }

    public function __destruct()
    {
        unset($this);
    }

    public function wypisz()
    {
        echo "Obiekt: " . get_class($this) . "<br>";
        echo "Imie: " . $this->getImie() . "<br>";
        echo "Nazwisko: " . $this->getNazwisko() . "<br>";
        echo "Pesel: " . $this->getPesel() . "<br>";
    }
}
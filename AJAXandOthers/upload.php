<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Upload</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>

</head>
<body>

<div class="header">
    <a href="../index.html">
        <img src="../logo.png"  alt="">
    </a>
    Upload
</div>

<div id="container">
    <div id="uploadInputs">
    <form action="upload.php" method="post" enctype="multipart/form-data">
        <label>
            File 1:<br><br>
            <input type="file" name="uploadFiles1"/>

            <br><br>File 2:<br><br>
            <input type="file" name="uploadFiles2"/>

            <br><br>File 3:<br><br>
            <input type="file" name="uploadFiles3"/>
        </label>
        <input type="submit" value="Upload">
    </form>

    <?php

    @$name_1 = $_FILES['uploadFiles1']['name'];
    @$size_1 = $_FILES['uploadFiles1']['size'];
    @$type_1 = $_FILES['uploadFiles1']['type'];
    @$tmp_name_1 = $_FILES['uploadFiles1']['tmp_name'];

    @$name2 = $_FILES['uploadFiles2']['name'];
    @$size2 = $_FILES['uploadFiles2']['size'];
    @$type2 = $_FILES['uploadFiles2']['type'];
    @$tmp_name2 = $_FILES['uploadFiles2']['tmp_name'];

    @$name3 = $_FILES['uploadFiles3']['name'];
    @$size3 = $_FILES['uploadFiles3']['size'];
    @$type3 = $_FILES['uploadFiles3']['type'];
    @$tmp_name3 = $_FILES['uploadFiles3']['tmp_name'];

    $location = "upload/";

    if(isset($name_1)) {
        if (!empty($name_1)) {
            if (move_uploaded_file($tmp_name_1, $location . $name_1)) {
                chmod($location.$name_1,0777);
                echo "<br>Pobrano plik 1";
            }
        }
    }
    if(isset($name2)) {
        if (!empty($name2)) {
            if (move_uploaded_file($tmp_name2, $location . $name2)) {
                chmod($location . $name2, 0777);
                echo "<br>Pobrano plik 2";
            }
        }
    }
    if(isset($name3)) {
        if (!empty($name3)) {
            if (move_uploaded_file($tmp_name3, $location . $name3)) {
                chmod($location.$name3,0777);
                echo "<br>Pobrano plik 3";
            }
        }
    }

    ?>
        <br/>
        <br/>
        <br/>
        <a href="fileList.php" style="text-align: center; font-size: 20px;" class="hreff">Lista dostępnych plików</a>
    </div>
    <div class="footer">
        <br/><br/><br/><br/><br/><br/>
        <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2Fupload.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="https://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2Fupload.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a class="hreff" href="view.php?file=upload.php" target="_blank">upload.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/style.css" target="_blank">style.css</a>
    </div>
</div>

</body>

</html>


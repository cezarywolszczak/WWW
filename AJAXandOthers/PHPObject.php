<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Obiektowy PHP</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>

</head>
<body>
<div class="header">
    <a href="../index.html">
        <img src="../logo.png" alt="">
    </a>
    Obiektowy PHP
</div>

<div id="container">
    <div class="objectResult">

        <?php

        include 'Osoba.php';
        include 'Student.php';
        $osoba = new Osoba("Adam", "Kowalski", 999999999);
        $student = new Student("Marek", "Kwiatkowski", 999111111, 6);

        echo "<h2>Obiekt Student</h2>";
        $student->wypisz();
        echo "<br>";

        echo "<h2>Obiekt Osoba</h2>";
        $osoba->wypisz();
        echo "<br>";

        echo "<h2>Obiekt Student po zserializowaniu</h2>";
        $studentSerial = serialize($student);
        print_r($studentSerial);
        echo "<br>";

        echo "<h2>Obiekt Student po zdeserializowaniu</h2>";
        $studentUnserial = unserialize($studentSerial);
        print_r($studentUnserial);
        echo "<br>";

        echo "<h2>Obiekt Osoba po zserializowaniu</h2>";
        $osobaSerial = serialize($osoba);
        print_r($osobaSerial);
        echo "<br>";

        echo "<h2>Obiekt Osoba po zdeserializowaniu</h2>";
        $osobaUnserial = unserialize($osobaSerial);
        print_r($osobaUnserial);
        echo "<br>";

        echo "<h2>Obiekt Student zostaje zniszczany przy użyciu destruktora</h2>";
        $student->__destruct();

        echo "<h2>Obiekt Osoba zostaje zniszczany przy użyciu destruktora</h2>";
        $osoba->__destruct();

        ?>

    </div>
    <div class="footer">
        <br/><br/>
        <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2Fupload.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="https://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FAJAXandOthers%2Fupload.php"
           target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a class="hreff" href="view.php?file=PHPObject.php" target="_blank">PHPObject.php</a>&nbsp;
        <a class="hreff" href="view.php?file=Student.php" target="_blank">Student.php</a>&nbsp;
        <a class="hreff" href="view.php?file=Osoba.php" target="_blank">Osoba.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/AJAXandOthers/style.css" target="_blank">style.css</a>
    </div>
</div>

</body>

</html>


<?php
    session_start();
    if(!isset($_SESSION['online'])){
        header('Location: logowanieFormularz.php');
        exit();
    }

    if(isset($_POST['sendButton'])){
        $_SESSION['name'] = (isset($_POST['name']))? $_POST['name'] : '';
        $_SESSION['surname'] = (isset($_POST['surname']))? $_POST['surname'] : '';
        $_SESSION['dataOfBirth'] = (isset($_POST['dataOfBirth']))? $_POST['dataOfBirth'] : '';
        $_SESSION['age'] = (isset($_POST['age']))? $_POST['age'] : '';
        $_SESSION['pesel'] = (isset($_POST['pesel']))? $_POST['pesel'] : '';
        $_SESSION['sex'] = (isset($_POST['sex']))? $_POST['sex'] : '';
        $_SESSION['studies'] = (isset($_POST['studies']))? $_POST['studies'] : '';
        $_SESSION['uploadPicture'] = (isset($_POST['uploadPicture']))? $_POST['uploadPicture'] : false;
        $_SESSION['comment'] = (isset($_POST['comment']))? $_POST['comment'] : false;
        $_SESSION['consent'] = (isset($_POST['consent']))? $_POST['consent'] : false;
    }
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Formularz</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="../tabelaFormularz/style.css" type="text/css"/>
    <link rel="stylesheet" href="style.css" type="text/css"/>
    <script src="formularz.js" type="text/javascript"></script>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>

    <link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

</head>
<body>

<div class = "wraper">


    <div class="header">
        <a href="../index.html">
            <img src="../logo.png"  alt="">
        </a>
        Formularz
    </div>
    <div>
        <a href="logout.php" id="logout">Wyloguj</a>
    </div>

    <div id="container">
        <form action="formularz.php" method="post">
            <div class="form">
                Imię:
                <span class="spanSpacing" style="letter-spacing: 48px">&nbsp;&nbsp;</span>
                <input name="name" id="name" type="text" title="name" style="width: 400px" onkeydown="validName()"/><br/><br/>

                Nazwisko:
                <span class="spanSpacing" style="letter-spacing: 31px">&nbsp;&nbsp;</span>
                <input name="surname" id="surname" type="text" title="surname" style="width: 400px" onkeydown="validSurname()"/><br/><br/>

                Data urodzenia:
                <span class="spanSpacing" style="letter-spacing: 15px">&nbsp;&nbsp;</span>
                <input name="dataOfBirth" id="dataOfBirth" type="text" title="data" style="width: 400px" onchange="age()"><br/><br/>

                Wiek:
                <span class="spanSpacing" style="letter-spacing: 48px">&nbsp;&nbsp;</span>
                <input name="age" id="age" type="text" title="age" readonly style="width: 400px" ><br/><br/>

                Pesel:
                <span class="spanSpacing" style="letter-spacing: 47px">&nbsp;&nbsp;</span>
                <input name="pesel" id="pesel" type="text" title="pesel" style="width: 400px" onkeydown="checkPesel()"/><br/><br/>

                Płeć:
                <span class="spanSpacing" style="letter-spacing: 51px">&nbsp;&nbsp;</span>
                <select name="sex" id="sex" title="sex">
                    <option id="male">Mężczyzna</option>
                    <option id="female">Kobieta</option>
                </select><br/><br/>

                Kierunek studiów:<br/><br/>
                <input name="studies" type="radio" title="it" checked/>Informatyka
                <input name="studies" type="radio" title="maths"/>Matematyka
                <input name="studies" type="radio" title="physic"/>Fizyka<br/><br/>

                Zdjęcie: &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <span class="spanSpacing" style="letter-spacing: 27px">&nbsp;&nbsp;</span>
                <input name="uploadPicture" id="uploadPicture" type="file" onchange="checkImageExtension()"><br/><br/>

                Komentarz:<br/><br/>
                <textarea name="comment" id="comment" title="comment" onkeydown="counter()"></textarea>
                <br/><br/>
                <div id="counter">Aktualna liczba liter w komętarzu: 0</div>


                <input name="consent" id="consent" type="checkbox" title="consent" onchange="checkConsent()"/>
                    <span id="consentCheckbox">
                        Zgadzam się na przetwarzanie moich danych osobowych w celu jakimś tam.
                    </span>
                <br/><br/>
                <input name="sendButton" id="sendButton" type="button" value="Wyślij" onclick="send()">

            </div>
        </form>
    </div>

    <div id="stick">
        Programowanie internetowe
    </div>
    <div class="footer">
        <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FmobilDesktopLogowanie%2Fformularz.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FmobilDesktopLogowanie%2Fformularz.php&profile=css3&usermedium=all&warning=1&vextwarning=&lang=pl-PL" target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a class="hreff" href="view.php?file=login.php" target="_blank">login.php</a>&nbsp;
        <a class="hreff" href="view.php?file=formularz.php" target="_blank">formularz.php</a>&nbsp;
        <a class="hreff" href="view.php?file=logout.php" target="_blank">logout.php</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/mobilDesktopLogowanie/style.css" target="_blank">style.css</a>&nbsp;
        <a class="hreff" href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/mobilDesktopLogowanie/formularz.js" target="_blank">formularz.js</a>&nbsp;
    </div>

    <script>
        $(function() {
            $( "#dataOfBirth" ).datepicker();
        });
    </script>

</div>

</body>
</html>
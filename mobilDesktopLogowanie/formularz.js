/**
 * Created by HP on 20.10.2015.
 */

function validName(){
    var name = document.getElementById("name").value;
    var tmp = true;
    if(name.length == 0)
        tmp = false;
    else if(name.charAt(0) != name.charAt(0).toUpperCase())
        tmp = false;
    for(var i = 0; i<name.length; i++){
        if(name.charAt(i) == " ")
            tmp = false;
    }
    if(tmp == false)
        document.getElementById("name").style.background = "#FF4A45";
    else
        document.getElementById("name").style.background = "white";
}

function validSurname(){
    var surname = document.getElementById("surname").value;
    var bool = true;
    if(surname.length == 0)
        bool = false;
    else if(surname.charAt(0) != surname.charAt(0).toUpperCase())
        bool = false;
    for(var i = 0; i<surname.length; i++){
        if(surname.charAt(i) == " ")
            bool = false;
    }
    if(bool == false)
        document.getElementById("surname").style.background = "#FF4A45";
    else
        document.getElementById("surname").style.background = "white";
}

function age(){
    var date = new Date();
    var year = date.getFullYear().valueOf();
    var month = date.getMonth().valueOf() + 1;

    var age;
    var date2 = document.getElementById("dataOfBirth").value;
    var year2 = "";
    var month2 = "";

    for(var i=0; i<date2.length;i++){
        if(i>5 && i<10)
            year2 = year2 + date2.charAt(i);
        else if(i<2)
            month2 = month2 + date2.charAt(i);
    }
    if(month2 > month)
        age = year - year2 - 1;
    else
        age = year - year2;

    document.getElementById("age").value = age;
}

function checkDateField(){
    var date = document.getElementById("dataOfBirth").value;
    if(date == "")
        document.getElementById("dataOfBirth").style.background = "#FF4A45";
    else
        document.getElementById("dataOfBirth").style.background = "white";
}

function checkPesel(){

    var bool = true;
    var pesel = document.getElementById("pesel").value;
    var date = document.getElementById("dataOfBirth").value;
    var year = "";
    var month = "";
    var day = "";

    for(var i=0; i<date.length; i++){
        if(i>5 && i<10)
            year = year + date.charAt(i);
        else if(i>2 && i<5)
            day = day + date.charAt(i);
        else if(i<2)
            month = month + date.charAt(i);
    }

    if(pesel.length != 11)
        bool = false;
    else{
        if((pesel.charAt(0)+pesel.charAt(1)) != (year.charAt(2)+year.charAt(3)))
            bool = false;
        else if((pesel.charAt(2)+pesel.charAt(3)) != month)
            bool = false;
        else if((pesel.charAt(4)+pesel.charAt(5)) != day)
            bool = false;
        else
            bool = true;
    }

    if(bool == false)
        document.getElementById("pesel").style.background = "#FF4A45";
    else
        document.getElementById("pesel").style.background = "white";
}

function checkImageExtension(){

    var img = document.getElementById("uploadPicture").value;
    if(img.length>4){
        var tmp =img.charAt(img.length-4)+""+ img.charAt(img.length-3)+"" + img.charAt(img.length - 2)+"" + img.charAt(img.length - 1);
        if ((tmp == ".png") || (tmp == ".jpg") || (tmp == ".tif"))
            document.getElementById("uploadPicture").style.background = "#8c8c8c";
        else
            document.getElementById("uploadPicture").style.background = "FF4A45";
    }else
        document.getElementById("uploadPicture").style.background = "#FF4A45";
}

function checkConsent(){

    var isSelected = document.getElementById("consent").checked;
    if(isSelected == false)
        document.getElementById("consentCheckbox").style.color = "#FF4A45";
    else
        document.getElementById("consentCheckbox").style.color = "white";

}

function counter(){

    var text = document.getElementById("comment").value;

    if(text.length == 0)
        document.getElementById("counter").innerHTML = "Aktualna liczba liter w komętarzu: 0";
    else
        document.getElementById("counter").innerHTML = "Aktualna liczba liter w komętarzu: " + (text.length+1);
}

function send(){
    validName();
    validSurname();
    age();
    checkDateField();
    checkPesel();
    checkConsent();
    checkImageExtension();
}
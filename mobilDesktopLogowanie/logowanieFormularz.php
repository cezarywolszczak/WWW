<?php
    session_start();
    if(isset($_SESSION['online']) && $_SESSION['online'] == true){
        header('Location: formularz.php');
        exit();
    }
?>
<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Logowanie</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>


</head>
<body>

    <div class="header">
        <a href="../index.html">
            <img src="../logo.png"  alt="">
        </a>
        Okno logowania
    </div>

    <form action="login.php" method="post">
        <div id="inData">
            <table>
                <tr>
                    <td>
                        Login
                    </td>
                    <td>
                       Hasło
                   </td>
                </tr>
                <tr>
                    <td>
                        <input type="text" name="login" title="">
                    </td>
                    <td>
                        <input type="password" name="password" title="">
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td>
                        <input type="submit" value="Zaloguj" name="singin">
                    </td>
                </tr>
            </table>
        </div>
    </form>
<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 27.11.2015
 * Time: 01:30
 */
    if(isset($_SESSION['error'])){
        echo $_SESSION['error'];
    }
?>


</body>

</html>


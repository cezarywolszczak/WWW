<?php
/**
 * Created by PhpStorm.
 * User: HP
 * Date: 04.12.2015
 * Time: 15:06
 */

$file=$_REQUEST['file'];

// USE: view.php?file=file_you_want_to_view.php



function highlight_and_hide ($source) {
    global $file;

    $extension=explode(".", $source);
    if ($extension[sizeof($extension)-1] != "php") { // No PHP, not allowed to see...
        //header("HTTP/1.0 403 Forbidden");
        //exit();
    }

    if(file_exists($source)) {
        $t_lines = file($source); // read the file into a string
    } else {
        header("HTTP/1.0 404 Not Found");
        exit();
    }

    echo '
        <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
            "http://www.w3.org/TR/2002/REC-xhtml1-20020801/DTD/xhtml1-transitional.dtd">
        <html>

        <head>
        <title>PHP Source Viewer</title>
        <link rel="icon" href="../logo.png">
        <style type="text/css" media="all">
            /*<![CDATA[*/

            html, body{
                text-align: center;
            }
            body * {
                margin: 0 auto;
            }
            #content {
                text-align: justify;
            }

            /*]]>*/
        </style>

        </head>
        <body>

        <div id="shadow"><div id="content">

        <br />
        <br />
    ';

    $size=filesize($file);
    $file_size = round($size / 1024 * 100) / 100 . "Kb";

    echo '
        This is a PHP
    <a href="viewsource.php?file=viewsource.php" onclick="window.open(this.href); return false">
      View Source
    </a>
    script.

    <br />
    <hr />
    <br />

    <small>File: <b>'.$file.'</b> Size: <b>'.$file_size.'</b></small><br /><br />
    ';

    // This function is part of PHP but the above one hide my secret stuff
    // $po=show_source($file);

    $step = 0;
    $str = "";

    foreach ($t_lines as $line) {
        switch ($step) {
            case 0: // nothing to hide
                if (stristr($line, "@@" . "begin_hide_code") ) { // found a begin hide token
                    $step = 1;
                    // $str .= "/* !!! BEGINNING OF THE HIDDEN CODE PART\n";
                } else {
                    $str .= $line;
                } // if
                break;
            case 1: // found a begin hide token
                if (stristr($line, "@@" . "end_hide_code") ) { // found an end hide token
                    $step = 0;
                    // $str .= "  !!! END OF THE HIDDEN CODE PART */\n";
                } else {
                    // $str .= "  ...\n";
                } // if
                break;
        } // switch
    } // for

    highlight_string($str);

    echo '
        <br />
        <br />
        </div>
        </div>
        </body>
        </html>
    ';

}

highlight_and_hide($file);

?>
<?php
    session_start();
?>

<!DOCTYPE html>
<html lang="pl">
<head>
    <meta charset="UTF-8">
    <meta name="discriptions" content="">
    <meta name="author" content="Cezary Wolszczak Elektryczny/Informatyka">
    <meta name="keywords" content="">
    <meta http-equiv="x-ua-compatible" content="IE=edge"/>
    <title>Logowanie</title>
    <link rel="icon" href="../logo.png">

    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="stylesheet" href="fontsMD/css/fontello.css" type="text/css"/>
    <link href='https://fonts.googleapis.com/css?family=Ubuntu:400,700' rel='stylesheet' type='text/css'>


</head>
<body>

    <div class="header">
        <a href="../index.html">
            <img src="../logo.png" alt="">
        </a>
        Wersja strony
    </div>

<?php
    /**
     * Created by PhpStorm.
     * User: HP
     * Date: 29.11.2015
     * Time: 11:41
     */

    echo $_SESSION['version'];
?>

    <div id="source">
        <a href="https://validator.w3.org/nu/?doc=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FmobilDesktopLogowanie%2FmobilDesktop.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-html401.png" alt=""> </a>&nbsp;
        <a href="http://jigsaw.w3.org/css-validator/validator?uri=http%3A%2F%2Fvolt.iem.pw.edu.pl%2F~wolszczc%2FmobilDesktopLogowanie%2FmobilDesktop.php" target="_blank"> <img src="http://www.w3.org/Icons/valid-css.png" alt=""> </a>&nbsp;
        <br/>Źródła strony:<br/>
        <a href="view.php?file=mobilDesktop.php" target="_blank">mobliDesktop.php</a> &nbsp;&nbsp;
        <a href="view.php?file=mobilDesktopLogic.php" target="_blank">mobilDesktopLogic.php</a>&nbsp;&nbsp;
        <a href="view-source:http://volt.iem.pw.edu.pl/~wolszczc/mobilDesktopLogowanie/style.css" target="_blank">style.css</a>&nbsp;&nbsp;

    </div>

</body>
</html>
